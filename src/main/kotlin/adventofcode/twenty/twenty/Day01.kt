package adventofcode.twenty.twenty

import java.io.File

class DayOne {
    private val data: List<Int> =
        File(ClassLoader.getSystemResource("day_01").file)
            .useLines { it.toList() }
            .map { it.toInt() }

    val partOne: Int = data.filter { i -> data.any { it + i == 2020 } }
        .reduce { acc, i -> acc * i }

    val partTwo: Int = data.filter { i -> data.any { j -> data.any { it + j + i == 2020 } } }
        .reduce { acc, i -> acc * i }
}

fun main(args: Array<String>) {
    println(DayOne().partOne)
    println(DayOne().partTwo)
}
