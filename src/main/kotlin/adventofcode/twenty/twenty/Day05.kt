package adventofcode.twenty.twenty

import java.io.File
import java.math.BigInteger

class DayFive {
    private val input: List<Int> =
        File(ClassLoader.getSystemResource("day_05").file)
            .useLines { it.toList() }
            .map { it.replace(Regex("[FL]"), "0") }
            .map { it.replace(Regex("[BR]"), "1") }
            .map { it.toInt(2) }

    fun partOne(): Int = input.max() ?: 0

    fun partTwo() = (input.min()!!..input.max()!!)
        .fold(BigInteger.ONE, multiply)
        .div(input.fold(BigInteger.ONE, multiply))

    private val multiply: (BigInteger, Int) -> BigInteger = { acc, i -> acc.times(i.toBigInteger()) }
}

fun main(args: Array<String>) {
    println(DayFive().partOne())
    println(DayFive().partTwo())
}
