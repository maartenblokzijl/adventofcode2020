package adventofcode.twenty.twenty

import adventofcode.twenty.twenty.DayEight.Operation.*
import java.io.File

class DayEight {

    data class Instruction(val index: Int, val operation: Operation, val argument: Int)
    enum class Operation { ACC, JMP, NOP }
    data class Result(val acc: Int, val success: Boolean = false)

    private val input: List<Instruction> =
        File(ClassLoader.getSystemResource("day_08").file)
            .useLines { it.toList() }
            .mapIndexed { index, line ->
                Instruction(
                    index,
                    Operation.valueOf(line.split(" ")[0].toUpperCase()),
                    line.split(" ")[1].toInt()
                )
            }

    private fun execute(
        instructions: List<Instruction> = input,
        index: Int = 0,
        acc: Int = 0,
        executed: List<Int> = listOf()
    ): Result =
        when {
            executed.any { it == index } -> Result(acc)
            index == input.lastIndex -> Result(acc, true)
            else -> instructions[index].let {
                when (it.operation) {
                    ACC -> execute(instructions, index + 1, acc + it.argument, executed.plus(index))
                    JMP -> execute(instructions, index + it.argument, acc, executed.plus(index))
                    NOP -> execute(instructions, index + 1, acc, executed.plus(index))
                }
            }
        }

    private fun findBug(
        instructions: List<Instruction> = input,
        index: Int = 0,
        result: Result = Result(0, false)
    ): Int =
        if (result.success) {
            result.acc
        } else {
            findBug(input.mapIndexed { i, instruction ->
                if (i == index) {
                    when (instruction.operation) {
                        JMP -> Instruction(instruction.index, NOP, instruction.argument)
                        NOP -> Instruction(instruction.index, JMP, instruction.argument)
                        ACC -> instruction
                    }
                } else {
                    instruction
                }
            }, index + 1, execute(instructions))
        }

    fun partOne() = execute().acc
    fun partTwo() = findBug()
}

fun main(args: Array<String>) {
    val dayEight = DayEight()
    println(dayEight.partOne())
    println(dayEight.partTwo())
}
