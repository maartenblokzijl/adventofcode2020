package adventofcode.twenty.twenty

import java.io.File

typealias Seats = List<List<Char>>
typealias Seat = Pair<Int, Int>

class DayEleven {

    private val input: Seats =
        File(ClassLoader.getSystemResource("day_11").file)
            .useLines { it.toList() }
            .map { it.toList() }

    private fun Seats.countOccupiedSeats(): Int =
        this.sumBy { it.count { seat -> seat == '#' } }

    private operator fun Seats.contains(seat: Seat): Boolean =
        seat.first in this.indices && seat.second in this.first().indices

    private operator fun Seat.plus(that: Seat): Seat =
        Seat(this.first + that.first, this.second + that.second)

    private val neighbors: Sequence<Seat> = sequenceOf(
        -1 to -1, -1 to 0, -1 to 1,
        0 to -1, 0 to 1,
        1 to -1, 1 to 0, 1 to 1
    )

    private fun countByNeighbors(seats: Seats, seat: Seat): Int =
        neighbors.map { it + seat }
            .filter { it in seats }
            .count { seats[it.first][it.second] == '#' }

    private fun countByVisible(seats: Seats, seat: Seat): Int =
        neighbors.mapNotNull { vector ->
            generateSequence(seat + vector) { vector + it }
                .takeWhile { it in seats }
                .map { seats[it.first][it.second] }
                .firstOrNull { it != '.' }
        }
            .count { it == '#' }

    private fun gameOfLife(
        seatsCounter: (Seats, Seat) -> Int,
        maximum: Int,
        current: Seats = input,
        next: MutableList<List<Char>> = mutableListOf()
    ): Seats =
        current.mapIndexedTo(next) { x, row ->
            row.mapIndexed { y, seat ->
                when {
                    seat == 'L' && seatsCounter(current, Seat(x, y)) == 0 -> '#'
                    seat == '#' && seatsCounter(current, Seat(x, y)) >= maximum -> 'L'
                    else -> seat
                }
            }
        }
            .let {
                if (current == next)
                    current
                else
                    gameOfLife(seatsCounter, maximum, next)
            }

    fun partOne() = gameOfLife(this::countByNeighbors, 4).countOccupiedSeats()

    fun partTwo() = gameOfLife(this::countByVisible, 5).countOccupiedSeats()

}

fun main(args: Array<String>) {
    val dayEleven = DayEleven()
    println(dayEleven.partOne())
    println(dayEleven.partTwo())
}
