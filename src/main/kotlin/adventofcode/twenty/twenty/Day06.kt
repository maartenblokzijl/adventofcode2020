package adventofcode.twenty.twenty

import java.io.File

class DaySix {
    private val input: List<Pair<Int, String>> =
        File(ClassLoader.getSystemResource("day_06").file)
            .useLines { it.toList() }
            .fold(mutableListOf()) { tail: MutableList<Pair<Int, String>>, line: String ->
                tail.apply {
                    when {
                        this.isEmpty().and(line.isNotBlank()) -> this.add(1 to line)
                        line.isBlank() -> this.add(0 to line)
                        else -> this[this.lastIndex] = last().first.plus(1) to "${last().second}$line"
                    }
                }
            }

    fun partOne() = input.map { it.second.toCharArray().distinct().size }.sum()

    fun partTwo() = input.map { pair ->
        pair.second.toCharArray()
            .groupBy { it }
            .filter { it.value.size == pair.first }
            .size
    }.sum()
}

fun main(args: Array<String>) {
    val daySix = DaySix()
    println(daySix.partOne())
    println(daySix.partTwo())
}
