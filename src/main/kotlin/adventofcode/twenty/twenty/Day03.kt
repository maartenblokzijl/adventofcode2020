package adventofcode.twenty.twenty

import java.io.File

class DayThree {
    private val input: List<String> =
        File(ClassLoader.getSystemResource("day_03").file)
            .useLines { it.toList() }

    private fun countTrees(stepsRight: Int, stepsDown: Int): Long =
        input
            .filterIndexed { i, _ -> i % stepsDown == 0 }
            .filterIndexed { i, line ->
                i != 0 && line[i * stepsRight % line.length] == '#'
            }
            .count().toLong()

    val partOne: Long = countTrees(3, 1)

    val partTwo: Long =
        countTrees(1, 1) *
                countTrees(3, 1) *
                countTrees(5, 1) *
                countTrees(7, 1) *
                countTrees(1, 2)

}

fun main(args: Array<String>) {
    println(DayThree().partOne)
    println(DayThree().partTwo)
}
