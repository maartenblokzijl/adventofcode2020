package adventofcode.twenty.twenty

import java.io.File

class DaySeven {

    private data class Rule(val containedIn: String, val amount: Int, val contains: String)

    private val input: List<Rule> =
        File(ClassLoader.getSystemResource("day_07").file)
            .useLines { it.toList() }
            .filterNot { it.contains("no other") }
            .flatMap {
                it.replace(Regex("""bags|bag|contain|,|\."""), "")
                    .split(Regex("""\s+"""))
                    .let { ruleToParse ->
                        ruleToParse.drop(2).windowed(3, 3)
                            .map { contains ->
                                Rule(
                                    ruleToParse.take(2).joinToString(" "),
                                    contains.first().toInt(),
                                    contains.drop(1).joinToString(" ")
                                )
                            }
                    }
            }

    private fun findBagsContaining(bag: String): List<String> =
        input
            .filter { it.contains == bag }
            .flatMap { findBagsContaining(it.containedIn) }
            .plus(bag)

    private fun findAmountOfBagsRequiredFor(bag: String): Int =
        input
            .filter { it.containedIn == bag }
            .sumBy { it.amount * findAmountOfBagsRequiredFor(it.contains) }
            .plus(1)


    fun partOne() = findBagsContaining("shiny gold").distinct().size - 1

    fun partTwo() = findAmountOfBagsRequiredFor("shiny gold") - 1

}

fun main(args: Array<String>) {
    val daySeven = DaySeven()
    println(daySeven.partOne())
    println(daySeven.partTwo())
}
