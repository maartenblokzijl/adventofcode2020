package adventofcode.twenty.twenty

import java.io.File

class DayTen {

    private val input: List<Int> =
        File(ClassLoader.getSystemResource("day_10").file)
            .useLines { it.toList() }
            .map { it.toInt() }
            .sorted()
            .let { listOf(0) + it + (it.last() + 3) }

    fun partOne() = input
        .asSequence()
        .zipWithNext { a, b -> b - a }
        .groupingBy { it }
        .eachCount()
        .let { it[1]!! * (it[3]!!) }

    fun partTwo() = input
        .drop(1)
        .fold(mapOf(0 to 1L)) { acc, i ->
            acc.plus(i to (1..3).map { lookBack ->
                acc.getOrDefault(i - lookBack, 0)
            }.sum())
        }
        .values.last()

}

fun main(args: Array<String>) {
    val dayTen = DayTen()
    println(dayTen.partOne())
    println(dayTen.partTwo())
}
