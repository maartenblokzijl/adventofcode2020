package adventofcode.twenty.twenty

import java.io.File

class DayFour {
    private val input: List<String> =
        File(ClassLoader.getSystemResource("day_04").file)
            .useLines { it.toList() }
            .fold(mutableListOf()) { tail: MutableList<String>, line: String ->
                tail.apply {
                    if (lastOrNull() == null || line.isBlank()) {
                        this.add(line)
                    } else {
                        this[this.lastIndex] = "${lastOrNull()} $line".trim()
                    }
                }
            }

    private val requiredFields: List<String> = listOf(
        "byr",
        "iyr",
        "eyr",
        "hgt",
        "hcl",
        "ecl",
        "pid"
    )

    fun partOne(): Long =
        input.filter { passport -> requiredFields.all { passport.contains(it) } }
            .count().toLong()

    fun partTwo(): Long =
        input.filter { passport -> requiredFields.all { passport.contains(it) } }
            .filter { passport ->
                passport.split(" ")
                    .all {
                        val key = it.split(":")[0]
                        val value = it.split(":")[1]
                        when (key) {
                            "byr" -> value.toIntOrNull() ?: 0 in 1920..2002
                            "iyr" -> value.toIntOrNull() ?: 0 in 2010..2020
                            "eyr" -> value.toIntOrNull() ?: 0 in 2020..2030
                            "hgt" -> when (value.substring(value.length - 2)) {
                                "cm" -> value.substring(0, value.length - 2).toIntOrNull() ?: 0 in 150..193
                                "in" -> value.substring(0, value.length - 2).toIntOrNull() ?: 0 in 59..76
                                else -> false
                            }
                            "hcl" -> value.matches("#[0-9a-z]{6}".toRegex())
                            "ecl" -> value.matches("(amb)|(blu)|(brn)|(gry)|(grn)|(hzl)|(oth)".toRegex())
                            "pid" -> value.matches("[0-9]{9}".toRegex())
                            else -> true
                        }
                    }
            }
            .count().toLong()

}

fun main(args: Array<String>) {
    println(DayFour().partOne())
    println(DayFour().partTwo())
}
