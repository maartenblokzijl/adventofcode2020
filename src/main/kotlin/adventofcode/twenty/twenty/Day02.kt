package adventofcode.twenty.twenty

import java.io.File

class DayTwo {
    private val data: List<String> =
        File(ClassLoader.getSystemResource("day_02").file)
            .useLines { it.toList() }

    val partOne: Int = data.map { it.split("[\\-\\s:]".toRegex()).filter { i -> i.isNotBlank() } }
        .filter { i -> i[3].count { it.toString() == i[2] } >= i[0].toInt() }
        .count { i -> i[3].count { it.toString() == i[2] } <= i[1].toInt() }

    val partTwo: Int = data.map { it.split("[\\-\\s:]".toRegex()).filter { i -> i.isNotBlank() } }
        .count { i -> (i[3][i[0].toInt() - 1].toString() == i[2]).xor(i[3][i[1].toInt() - 1].toString() == i[2]) }
}

fun main(args: Array<String>) {
    println(DayTwo().partTwo)
    println(DayTwo().partOne)
}
